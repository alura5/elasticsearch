# Elasticsearch: Pesquisando e analisando os seus dados

Carga Horária: 20h

## Ambiente

### Elasticsearch
Para montagem do ambiente sera usado o ElasticSearch na versão [2.3.1](https://s3.amazonaws.com/caelum-online-public/elasticsearch/downloads/elasticsearch-2.3.1.zip) que deve ser instalado/descompactado em uma pasta com permissão de leitura e escrita. Tem como pré-requisito a existencia de um JVM ja instalada com a variavel de ambiente *JAVA_HOME* configurada.

Para testar a instalação basta executar o arquivo .bat ou .sh contido na pasta /bin da instalação
```.shell
PS C:\Program Files\elasticsearch-2.3.1\bin> ./elasticsearch.bat
[2021-05-31 14:40:57,517][INFO ][node                     ] [Nezarr the Calculator] version[2.3.1], pid[8852], build[bd98092/2016-04-04T12:25:05Z]
[2021-05-31 14:40:57,518][INFO ][node                     ] [Nezarr the Calculator] initializing ...
[2021-05-31 14:40:57,833][INFO ][plugins                  ] [Nezarr the Calculator] modules [reindex, lang-expression, lang-groovy], plugins [kopf], sites [kopf]
[2021-05-31 14:40:57,846][INFO ][env                      ] [Nezarr the Calculator] using [1] data paths, mounts [[(C:)]], net usable_space [115.8gb], net total_space [465.1gb], spins? [unknown], types [NTFS]
[2021-05-31 14:40:57,846][INFO ][env                      ] [Nezarr the Calculator] heap size [910.5mb], compressed ordinary object pointers [true]
[2021-05-31 14:40:59,069][INFO ][node                     ] [Nezarr the Calculator] initialized
[2021-05-31 14:40:59,069][INFO ][node                     ] [Nezarr the Calculator] starting ...
[2021-05-31 14:40:59,576][INFO ][transport                ] [Nezarr the Calculator] publish_address {127.0.0.1:9300}, bound_addresses {127.0.0.1:9300}, {[::1]:9300}
[2021-05-31 14:40:59,579][INFO ][discovery                ] [Nezarr the Calculator] elasticsearch/fw0-I3_bTv24G1v8zyb96g
[2021-05-31 14:41:04,115][INFO ][cluster.service          ] [Nezarr the Calculator] new_master {Nezarr the Calculator}{fw0-I3_bTv24G1v8zyb96g}{127.0.0.1}{127.0.0.1:9300}, reason: zen-disco-join(elected_as_master, [0] joins received)
[2021-05-31 14:41:04,138][INFO ][gateway                  ] [Nezarr the Calculator] recovered [0] indices into cluster_state
[2021-05-31 14:41:04,573][INFO ][http                     ] [Nezarr the Calculator] publish_address {127.0.0.1:9200}, bound_addresses {127.0.0.1:9200}, {[::1]:9200}
[2021-05-31 14:41:04,573][INFO ][node                     ] [Nezarr the Calculator] started
```
Consultando o endereço de publicação 127.0.0.1:9200 deve apresentar um resultado como este:
```.shell
curl 127.0.0.1:9200
```
```.json
{
  "name" : "Nezarr the Calculator",
  "cluster_name" : "elasticsearch",
  "version" : {
    "number" : "2.3.1",
    "build_hash" : "bd980929010aef404e7cb0843e61d0665269fc39",
    "build_timestamp" : "2016-04-04T12:25:05Z",
    "build_snapshot" : false,
    "lucene_version" : "5.5.0"
  },
  "tagline" : "You Know, for Search"
}
```

### Kopf
Para facilitar a interação com a instancia do ElasticSearch durante o curso, sera usado o plugin Kopf na versao [2.1.2](https://s3.amazonaws.com/caelum-online-public/elasticsearch/downloads/elasticsearch-kopf-2.1.2.zip). Para instalação basta baixar o zip e extrair, movendo o resultado para a pasta plugins dentro da instalação do ElasticSearch.
Reestartando a instancia do Elastic é possivel acessar http://localhost:9200/_plugin/kopf/.


### Primeiras interações com ElasticSearch

```
curl -XPOST 'http://localhost:9200/catalogo/pessoas' -d '{
    "nome" : "João Silva",
    "interesses" : ["futebol", "música", "literatura"],
    "cidade" : "São Paulo",
    "formação" : "Letras",
    "estado" : "SP",
    "país" : "Brasil"
}'
```


### Criando documentos com identificadores

```
/catalogo/pessoas/1
{
    "nome": "João Silva",
    "interesses": ["futebol", "música", "literatura"],
    "cidade": "São Paulo",
    "formação": "Letras",
    "estado": "SP",
    "país": "Brasil"
}

/catalogo/pessoas/2
{
    "nome": "Maria Silva",
    "interesses": ["pintura", "literatura", "teatro"],
    "cidade": "Diamantina",
    "formação": "Artes Plásticas",
    "estado": "MG",
    "país": "Brasil"
}

/catalogo/pessoas/3
{
    "nome": "Richard Edward",
    "interesses": ["matemática", "física", "música"],
    "cidade": "Boston",
    "formação": "Física",
    "estado": "MA",
    "país": "Estados Unidos"
}

/catalogo/pessoas/4
{
    "nome": "Patrick von Steppat",
    "interesses": ["computação", "culinária", "cinema"],
    "cidade": "Rio de Janeiro",
    "formação": "Gastronomia",
    "estado": "RJ",
    "país": "Brasil"
}
```

### Localização de documentos

Localize o documento de identificador 3.

```
curl -XGET 'http://localhost:9200/catalogo/pessoas/3'
```

### Listando todos os tipos de documento

Agora liste todos os documentos do índice catalogo usando o tipo pessoas

```
curl -XGET 'http://localhost:9200/catalogo/pessoas/_search'
```


## Indices, tipos, shards e replicas

### Nomenclatura 
| Banco Relacional | ElasticSearch |
|-|-|
| Instance | Index |
| Table | Type |
| Schema | Mapping |
| Tuple | Document |
| Collumn | Atribute |

### Fazendo o crud da aplicação

Descobrindo a existencia ou não de um document
```
curl -XHEAD -v 'http://localhost:9200/catalogo/pessoas/1' 
```

Atualizando parcialmente um document
```
curl -XPOST 'http://localhost:9200/catalogo/pessoas/1/_update' -d '{
  "doc": {
    "nome": "João Pedro"
  }
}'
```



Links uteis
* ElasticSearch na versão 2.3.1 ([elasticsearch-2.3.1.zip](https://s3.amazonaws.com/caelum-online-public/elasticsearch/downloads/elasticsearch-2.3.1.zip))
* Kopf na versão 2.1.2 ([elasticsearch-kopf-2.1.2.zip](https://s3.amazonaws.com/caelum-online-public/elasticsearch/downloads/elasticsearch-kopf-2.1.2.zip))
* [Apache Solr. vs ElasticSearch](http://solr-vs-elasticsearch.com/)

